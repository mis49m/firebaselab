package com.mis49m.firebaselab;

import java.io.Serializable;

public class Contact implements Serializable {

    String key;

    String name;
    String phone;

    public Contact() {

    }

    public Contact(String key, String name, String phone) {
        this.key = key;
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
